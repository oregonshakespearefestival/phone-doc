## Avaya 9611g Group Sales Phone Guide

### Button Features
----

       --------------------------------------------
    1. | Line 1             |    Park Group Sales | 5.
       --------------------------------------------
    2. | Line 2             |   Aux On Box Office | 6.
       --------------------------------------------
    3. | Line 3             |  Aux On Group Sales | 7.
       --------------------------------------------
    4. | Dial Supervisor    |Availible Supervisor | 8.
       --------------------------------------------

1. Call line 1, press this button to place a call out.  
2. Call line 2, press this button to place a call out.  
3. Call line 3, press this button to place a call out.  
4. Dial Supervisor, Calls extension 206 using an availible line  
5. Pressing button 5 places your active call in group sales call park  
   The call can be picked up by anyone in the group sales.  
6. When the button is lit you are in the box office hunt group.  
7. When the button is lit you are in the group sales hunt group.  
8. Dials the Box Office Supervisor Hunt Group.  

### Phone Lines Explained
----
When you receive a call it will use the first availible line. With one active call you can still receive or place 2 additional calls.  If you have a call from the hunt group you will not receive an additional call from the hunt group until you have finished your current call.

### Putting Calls on Hold
----
There are four buttons along the bottom of the screen. When a call is active the left-most button will place the active call on hold. Pressing the button next to the line that is on hold will take the call off hold.

### Transfering Calls
----
To transfer a call press the `Transfer` button (3rd from the left) on an active call and type in the extension you want to transfer the call to. This will start a call to the extension you want to transfer to.  Once the person picks up press the `Complete` button (left-most button) to transfer the call to them.

### Ending Calls
----
To end the current call press the right-most button which is labeled `Drop`.

### Parking Calls
----
A call park, like park box office (button 5) is a shared hold for the whole box office. Only one call can be on it at a time, but anyone with the park can pickup the call.

### Disabling Phone Button Sounds
----
Press the `Home` button which is located above the dial pad. Select Options and `Settings > Screen & Sound Options`. Here you can toggle button clicks by pressing the Button Clicks button.

### Logging in as a different user
----
Press the `Features` button and select Phone User.  Then select `login` and input the extension you wish to login as. Enter the security pin of the extension, by default this is `123456`. Finally enter push the `Done` button.
##### Note: this will only work for IP Extensions.  
